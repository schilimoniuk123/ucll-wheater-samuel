package be.ucll.java.mobile.ucllweather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.google.gson.Gson;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import be.ucll.java.mobile.ucllweather.model.Geoname;
import be.ucll.java.mobile.ucllweather.model.PlaceSearch;
import be.ucll.java.mobile.ucllweather.model.WeatherObservation;
import be.ucll.java.mobile.ucllweather.model.WeatherSearch;


public class MainActivity extends AppCompatActivity implements Response.Listener, Response.ErrorListener{
    private static final String TAG = "MainActivity";
    private TextView txtSearch;
    private TextView txtCity;
    private TextView txtCountry;
    private TextView txtTemperature;
    private TextView txtPressure;
    private TextView txtHumidity;

    private String txtLng;
    private String txtLat;

    Geoname geoname;
    WeatherObservation weather;

    //2 parts of URL needed if searchterm is in middle of the URL
    private static final String SEARCH_PLACE_URL_PART1= "http://api.geonames.org/searchJSON?q="; //+ "&maxRows=1&username=jodieorourke";
    private static final String SEARCH_PlACE_URL_PART2= "&maxRows=1&username=coppers"; //used other username because own username wasn't validated yet

    private static final String SEARCH_WEATHER_URL_PART1 = "http://api.geonames.org/findNearByWeatherJSON?lat=";
    private static final String SEARCH_WEATHER_URL_PART2 = "&lng=";
    private static final String SEARCH_WEATHER_URL_PART3 = "&username=coppers";


    private String searchLink;
    private RequestQueue queue;
    private List<Geoname> geonames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtSearch = findViewById(R.id.txtSearch);

        txtCity = findViewById(R.id.txtCity);
        txtCountry = findViewById(R.id.txtCountry);

        txtTemperature = findViewById(R.id.txtTemperature);
        txtPressure = findViewById(R.id.txtPressure);
        txtHumidity = findViewById(R.id.txtHumidity);
        geoname = new Geoname();
    }
    public void onClickButtonSearch(View view)
    {
        queue = Volley.newRequestQueue(this);
        String searchTerm = txtSearch.getText().toString();

        if (!searchTerm.isEmpty())
        {
            try {
                searchTerm = URLEncoder.encode(searchTerm, "UTF-8");
                searchLink = SEARCH_PLACE_URL_PART1 + searchTerm + SEARCH_PlACE_URL_PART2;
            }catch(Exception e){}

            Log.d(TAG, "URL: " + searchLink);
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, searchLink, null, this, this);
            queue.add(req);
        }
        else
        {
            Toast.makeText(this, "please type in a city name", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onResponse(Object response)
    {
        // Cast into Gson JSONObject
        JSONObject jsono = (JSONObject) response;
        Log.d(TAG, jsono.toString());

        PlaceSearch omdbrespo = new Gson().fromJson(jsono.toString(), PlaceSearch.class);
        if (omdbrespo != null && omdbrespo.getGeonames() != null && omdbrespo.getGeonames().size() > 0)
        {
            geonames = new ArrayList<>(omdbrespo.getGeonames().size());
            geonames = omdbrespo.getGeonames();
            if (geonames!=null)
            {
                Geoname geoname = geonames.get(0);
                txtCity.setText(geoname.getToponymName());
                txtCountry.setText(geoname.getCountryName());
                txtLat = geoname.getLat();
                txtLng = geoname.getLng();
            }
        }

        queue = Volley.newRequestQueue(this);
        try {
            txtLng = URLEncoder.encode(txtLng, "UTF-8");
            txtLat = URLEncoder.encode(txtLat, "UTF-8");
            searchLink = SEARCH_WEATHER_URL_PART1+ txtLat + SEARCH_WEATHER_URL_PART2 + txtLng + SEARCH_WEATHER_URL_PART3;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        WeatherSearch omdbrespo_ = new Gson().fromJson(jsono.toString(), WeatherSearch.class);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, searchLink, null, this, this);
        queue.add(req);

        weather = new WeatherObservation();
        weather = omdbrespo_.getWeatherObservation();

        if (omdbrespo_ != null && omdbrespo_.getWeatherObservation() != null && weather.getHectoPascAltimeter() != null) {
            weatherSetter(weather);
        }
    }

    public void weatherSetter(WeatherObservation weather)
    {
        if (weather.getTemperature() != null)
        {
            txtTemperature.setText(weather.getTemperature() + " \u2103");
        }
        else
        {
            txtTemperature.setText("NA");
        }
        if (weather.getHectoPascAltimeter() != null)
        {
            txtPressure.setText(weather.getHectoPascAltimeter() + " Hp");
        }
        else
        {
            txtPressure.setText("NA");
        }
        if (weather.getHumidity() != null)
        {
            txtHumidity.setText(weather.getHumidity() + " %");
        }
    }



    @Override
    public void onErrorResponse(VolleyError error)
    {
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }
}